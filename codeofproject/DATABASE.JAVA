package resturan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class DataBase {

	public static Object db;
	private Connection connection = null;
	private ResultSet resultset = null;
	private Statement stmt = null;

	public Connection GetConnection() {
		return (connection);
	}

	public ResultSet GetResultSet() {
		return (resultset);
	}

	public boolean setConnection() {
		try {
			String username = "sa";
			String pass = "93149022";
			String url="jdbc:sqlserver://DESKTOP-7532DDQ\\MARYAM;DatabaseName=resturan1;";
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
			connection = DriverManager.getConnection(url, username, pass);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	public boolean selectQuery(String select_query) {
		if (setConnection()) {
			try {
				stmt = connection.createStatement();
				resultset = stmt.executeQuery(select_query);
				return true;
			} catch (Exception e) {
				return false;
			}
		} else {
			JOptionPane.showMessageDialog(null, "به پایگاهداده متصل نشد");
			return false;
		}
	}

	public boolean Query(String _Query) {
		if (setConnection()) {
			try {
				stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				resultset = stmt.executeQuery(_Query);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}

	public boolean updateQuery(String update_query, String ErrorMassage) {
		if (setConnection()) {
			try {
				stmt = connection.createStatement();
				stmt.executeUpdate(update_query);
				return true;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, ErrorMassage);
				return false;
			}
		}
		return false;
	}

	public boolean setDisconnect() {
		try {
			resultset.close();
			resultset = null;
			stmt.close();
			stmt = null;
			connection.close();
			connection = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public TableModel CreateTable(String _QueryTableSet) throws SQLException {
		DefaultTableModel dTModel=null;
		if (setConnection()) {
			try {
				Vector<Vector<Object>> data = new Vector<Vector<Object>>();
				Vector<String> columnNames = new Vector<String>();
				
				selectQuery(_QueryTableSet);
				ResultSetMetaData md = resultset.getMetaData();
				int columns = md.getColumnCount();
				for (int i = 0; i < columns; i++)
					columnNames.addElement(md.getColumnLabel(i + 1));

				while (resultset.next()) {
					Vector<Object> row = new Vector<Object>(columns);
					for (int i = 1; i <= columns; i++) {
						row.addElement(resultset.getObject(i));
					}
					data.addElement(row);

				}
				if (setDisconnect())
					dTModel= new DefaultTableModel(data, columnNames);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
	return dTModel;

	}

	public Vector<Object> createVector(String _query) throws SQLException {
		Vector<Object> newVector = new Vector<>();
		//newVector = null;
		if (setConnection()) {
			selectQuery(_query);
			while (resultset.next()) {
				newVector.addElement(resultset.getString(1));
			}
		}
		setDisconnect();
		return newVector;

	}

	public void setConnection1() {
		// TODO Auto-generated method stub
		
	}
}
