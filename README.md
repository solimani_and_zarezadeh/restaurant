# نرم افزار رستوران 
* به کمک نرم افزار رستوران مشتریان میتوانند منوی غذایی و قیمت و تعداد غذاهای موجود در رستوران را مشاهده کنند.
* صندوقدار به کمک این نرم افزار مشتریان را با نام کاربری و رمز عبور خاص خود ثبت میکند.


## ویژگی های کلیدی نرم افزار رستوران 
* •	امکان وارد کردن اطلاعات غذاها
* •	امکان ثبت اطلاعات مشتری
* •	امکان مشاهده منوی غذایی و قیمت و تعداد غذاها

## تحلیل و طراحی پروژه
* •	در ابتدا به بررسی چند عملکرد اصلی در نرم‌افزار در قالب سناریو عملکرد آزمون دهنده و سناریو عملکرد مدرس پرداخته ایم. 
* • [سناریوی مشتری] (documentation/scenario/SCENARIOMOSHTARI.md)
* •	[سناریوی رزرو غذا] (documentation/scenario/SCENARIOREZERGHAZA.md)
* • [سناریوی صندوقدار] (documentation/scenario/SCENARIOSANDOGHDAR.md)
* • [سناریوی کلی] (documentation/scenario/SCENARIO.md)
* [usecasediagram] (documentation/USECASE.md)
* [requirements] (documentation/REQUIREMENTS.md)
* [classdiagram] (documentation/class/CLASS.md)
* [statediagram] (documentation/state/STATE.md)
* [sequencediagram] (documentation/sequence/SEQUENCE.md)
* [database] (documentation/database/DATABASE.md)

## فازهای توسعه پروژه
* 1. شناخت کلی نرم افزار
* 2. نیازمندی ها
* 3. صندوقدار
* 4. مشتری
* 5. رزرو غذا
* 6. نرم افزار نهایی رستوران


## توسعه دهندگان
|نام و نام خانوادگی|   ID     |
|:----------------:|:--------:|
| مریم سلیمانی | @maryamsolimani |
| باهره زارع زاده| @baherezarezade |

